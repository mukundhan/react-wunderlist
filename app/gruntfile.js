module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        connect: {
            example: {
                port: 8000,
                base: ''
            }
        },
        wiredep: {
            target: {
                src: 'index.html'
            }
        }
    });
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-connect');
    grunt.registerTask('default', 'connect:example');
};