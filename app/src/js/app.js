import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";

import Index from "./pages/Index";
import MainContent from "./components/MainContent";
import SubTask from "./components/SubTask";

import Store from "./store/Store";

const data = {
  "mainLists": [
    {
      "name": "Inbox",
      "id": 15059024912343123,
      "mainTasks": []
    }, {
      "name": "Inbox",
      "id": 1505902491234983,
      "mainTasks": []
    }, {
      "name": "Today",
      "id": 1505902494983,
      "mainTasks": []
    }, {
      "name": "Buy Items",
      "mainTasks": [
        {
          "name": "Pen",
          "subTasks": [
            {
              "name": "get a red pen",
              "id": 1507142445623,
              "completed": true
            }, {
              "name": "write tasks",
              "id": 1507142446526,
              "completed": false
            }, {
              "name": "end date",
              "id": 1507142446550,
              "completed": true
            }, {
              "name": "check for updates",
              "id": 1507142449718,
              "completed": true
            }

          ],
          "createdOn": "2017-09-20T10:15:01.825Z",
          "dueDate": "2017-10-20T10:15:01.825Z",
          "completed": true,
          "isImportant": true,
          "remindMe": 900,
          "id": 1505902501826
        }, {
          "name": "Washing Machine",
          "subtasks": [],
          "createdOn": "2017-09-20T10:15:02.818Z",
          "completed": true,
          "dueDate": "2017-10-20T10:15:01.825Z",
          "isImportant": false,
          "remindMe": 900,
          "id": 1505902502818,
          "subTasks": [
            {
              "name": "pen",
              "id": 1507142445623,
              "completed": true
            }, {
              "name": "cycle",
              "id": 1507142446526,
              "completed": false
            }, {
              "name": "end date",
              "id": 1507142446550,
              "completed": true
            }, {
              "name": "check for updates",
              "id": 1507142449718,
              "completed": true
            }

          ]
        }, {
          "name": "Fridge",
          "subtasks": [],
          "createdOn": "2017-09-20T10:15:03.074Z",
          "completed": false,
          "dueDate": "2017-10-20T10:15:01.825Z",
          "isImportant": true,
          "remindMe": 900,
          "id": 1505902503074,
          "subTasks": [
            {
              "name": "Car",
              "id": 1507142445623,
              "completed": true
            }, {
              "name": "book",
              "id": 1507142446526,
              "completed": false
            }, {
              "name": "end date",
              "id": 1507142446550,
              "completed": true
            }, {
              "name": "check for updates",
              "id": 1507142449718,
              "completed": true
            }

          ]
        }

      ],
      "id": 1505902496929
    }
  ]
};
var store = new Store(data);
function persistMainList(mainLists) {
  data.mainLists = mainLists;
  console.log("after Saved main Lists", data.mainLists);
}
console.log("Store in app.js", Store.mainLists);
const app = document.getElementById('app');
ReactDOM.render(
  <Router history={hashHistory}>
  <Route path="/" component={Index} mainLists={Store.mainLists}></Route>
</Router>, app);