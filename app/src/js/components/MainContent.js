import React from "react";
import MainTask from "./MainTask"
import SubTask from "./SubTask"
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import Store from "../store/Store";
export default class MainContent extends React.Component {
  constructor(props) {
    super(props);
    console.log("Store in mainContent", Store);
    console.log("Store in mainContent", Store.data);
    this.state = {
      mainLists: Store.mainLists,
      currentMainList: this.props.currentMainList,
      mainTask: null
    };
    let reRender = true;
    console.log("currentMainList", this.state.currentMainList);
  }
  setMainList(mainListId) {
    let mainList = this
      .state
      .mainLists
      .filter((tempList) => {
        return tempList.id == mainListId;
      })
    console.log("founded mainList", mainList);
    this.setState((prevState, props) => ({currentMainList: mainList[0], mainLists: this.state.mainLists, mainTask: null}));
  }
  setTaskStatus(event) {
    let currentMainList;
    let currentMainTask;
    console.log(event.target.checked);
    this
      .state
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == event.target.id) {
                task.completed = event.target.checked;
                currentMainTask = task;
              }
            });
          currentMainList = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: currentMainList, mainTask: currentMainTask})
  }
  renderSubtask(event) {
    console.log("called", event.target.id);
    this
      .state
      .currentMainList
      .mainTasks
      .forEach((task) => {
        if (task.id == event.target.id) {
          this.setState({mainTask: task});
        }
      }, this)
  }

  setTaskImportant(event) {
    let mainList;
    let currentMainTask;
    console.log(event.target.checked);
    this
      .state
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == event.target.id) {
                task.isImportant = !event
                  .target
                  .classList
                  .contains('bg-red');
                currentMainTask = task;
              }
            });
          mainList = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainList, mainTask: currentMainTask})
  }

  setTaskReminder(value) {
    let mainList;
    let currentMainTask;
    console.log("reminder date", value);
    this
      .state
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == this.state.mainTask.id) {
                task.remindMe = value;
                currentMainTask = task;
              }
            });
          mainList = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainList, mainTask: currentMainTask})
  }

  setTaskDueDate(value) {
    let mainList;
    let currentMainTask;
    console.log("due date", value.toString());
    this
      .state
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == this.state.mainTask.id) {
                task.dueDate = value.toString();
                currentMainTask = task;
              }
            });
          mainList = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainList, mainTask: currentMainTask})
  }

  componentWillUpdate() {
    console.log("state in will Update", this.state);
    console.log("willMount Called");
  }

  componentWillMount() {
    console.log("willMount Called");
  }

  componentDidMount() {
    console.log("componentDidMount Called");
  }

  componentWillReceiveProps(props) {
    console.log("Store in mainContent", Store.getMainLists);
    if (props.currentMainList) {
      this.setState({currentMainList: props.currentMainList})
    }
    console.log("componentWillReceiveProps Called");
    console.log("props", props);
  }

  shouldComponentUpdate() {
    console.log("shouldComponentUpdate Called");
    return true;
  }

  componentWillUnmount() {
    console.log("componentWillUnmount Called");
  }

  deleteTask(id) {
    let taskIndex;
    let mainList;
    console.log(id);
    this
      .state
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task, tempIndex) => {
              if (task.id == id) {
                taskIndex = tempIndex;
              }
            });
          element
            .mainTasks
            .splice(taskIndex, 1);
          mainList = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainList, mainTask: null})
  }
  render() {
    console.log("before render in MainContent state = ", this.state);
    console.log("before render in MainContent Props = ", this.props);
    return (
      <div
        id="main-content"
        class="row col-sm-9 col-md-9 col-xs-12 col-lg-9 main-content">
        <MainTask
          renderSubtask={(event) => this.renderSubtask(event)}
          currentMainList={this.state.currentMainList}/>
        <SubTask
          actions={{
          setTaskReminder: (event) => this.setTaskReminder(event),
          deleteTask: (event) => this.deleteTask(event),
          deleteTask: (event) => this.deleteTask(event),
          setTaskStatus: (event) => this.setTaskStatus(event),
          setTaskImportant: (event) => this.setTaskImportant(event),
          setTaskDueDate: (event) => this.setTaskDueDate(event)
        }}
          mainTask={this.state.mainTask}
          mainList={this.state.currentMainList}></SubTask>
      </div>
    );
  }
}