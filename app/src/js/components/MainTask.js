import React from "react";
import ReactDOM from "react-dom";
import {IndexLink, Link} from "react-router";
import TaskElement from "./TaskElement";
import Store from "../store/Store";
export default class MainTask extends React.Component {
  constructor(props) {
    super(props);
    let currentMainListname = "";
    let newMainTaskName;
    this.state = {
      currentMainList: this.props.currentMainList
    };

    console.log("currentMainList", this.state.currentMainList)
  }

  addNewMainTask() {
    if (this.newMainTaskName) {
      let mainTask = {
        name: this.newMainTaskName,
        id: new Date().valueOf(),
        createdOn: new Date().toISOString(),
        dueDate: new Date().toISOString(),
        remindMe: "",
        subTasks: []
      }
      this
        .state
        .currentMainList
        .mainTasks
        .push(mainTask);
      ReactDOM
        .findDOMNode(this.refs.mainTaskInput)
        .value = '';
      this.persistMainList(this.state.currentMainList);
      this.setState({currentMainList: this.state.currentMainList})
    }
  }
  persistMainList(mainList) {
    Store.setMainLists(mainList);
  }

  _setMainListName(mainList) {
    if (mainList && mainList.name) {
      this.currentMainListName = mainList.name;
    }
  }
  _onEnter(event) {
    return event.key == "Enter";
  }

  _setMainTaskName(event) {
    this.newMainTaskName = event.target.value;
    if (this._onEnter(event)) {
      this.addNewMainTask();
    }
    console.log("mainTaskName", this.newMainTaskName);
  }

  componentWillUpdate() {
    console.log("willMount Called");
  }

  componentWillMount() {
    console.log("willMount Called");
  }
  componentDidMount() {
    console.log("componentDidMount Called");
  }

  componentWillReceiveProps(props) {
    console.log("componentWillReceiveProps Called");
    console.log("props in mainTasks", props);
    this._setMainListName(props.currentMainList);
    this.setState((prevState, props) => ({currentMainList: props.currentMainList}));
  }

  shouldComponentUpdate() {
    console.log("shouldComponentUpdate Called");
    return true;
  }
  setTaskImportant(event) {
    let mainTask;
    console.log(event.target.checked);
    this
      .props
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == event.target.id) {
                task.isImportant = !event
                  .target
                  .classList
                  .contains('bg-red');
              }
            });
          mainTask = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainTask})
  }
  setTaskStatus(event) {
    let mainTask;
    console.log(event.target.checked);
    this
      .props
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == this.state.currentMainList.id) {
          element
            .mainTasks
            .forEach((task) => {
              if (task.id == event.target.id) {
                task.completed = event.target.checked;
              }
            });
          mainTask = element;
        }
      });
    Store.setMainLists(this.state.mainLists);
    this.setState({currentMainList: mainTask})
  }

  componentWillUnmount() {
    console.log("componentWillUnmount Called");
  }
  render() {
    let unFinishedTaskNode;
    let finishedTaskNode;
    let mainTasks;
    let mainListName = "";
    const mainList = this.state.currentMainList;
    if (this.currentMainListName) {
      mainListName = this.currentMainListName;
    }
    return (
      <div className="col-sm-6 col-md-6 col-xs-6 col-lg-6 main-task">
        <input
          id="main-task-add"
          ref="mainTaskInput"
          onKeyPress={(event) => this._setMainTaskName(event)}
          className="add-task-btn"
          type="text"
          placeholder={"Add a Task " + mainListName}/>
        <TaskElement
          finished="false"
          onClick={this.props.renderSubtask}
          renderSubtask={this.props.renderSubtask}
          currentMainList={this.state.currentMainList}
          setTaskStatus=
          {(event) => this.setTaskStatus(event)}
          setTaskImportant={(event) => this.setTaskImportant(event)}></TaskElement>
        <input
          id="finished-task-add"
          className="add-task-btn"
          type="text"
          placeholder="Finished Tasks"
          readOnly/>
        <TaskElement
          finished="true"
          onClick={this.props.renderSubtask}
          renderSubtask={this.props.renderSubtask}
          currentMainList={this.state.currentMainList}
          setTaskStatus=
          {(event) => this.setTaskStatus(event)}
          setTaskImportant={(event) => this.setTaskImportant(event)}></TaskElement>
      </div>
    );
  }
}