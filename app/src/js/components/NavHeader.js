import React from "react";
export default class NavHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="navbar-header">
                <div className="brand-wrapper">
                    <button type="button" className="navbar-toggle" onClick={this.props.toggle}>
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <div className="brand-name-wrapper">
                        <a className="navbar-brand" href="#">
                            <span className="logo"></span>
                            WunderList
                        </a>
                        <a
                            data-toggle="collapse"
                            href="#search"
                            className="toggle btn btn-default hidden"
                            id="addListTrigger">
                            <i
                                className="fa fa-plus"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Add List!"></i>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
