import React from 'react';
import ReactDOM from 'react-dom';
import {IndexLink, Link} from 'react-router';
import NavHeader from './NavHeader';
import Store from '../store/Store'
export default class SideNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: true,
      mainLists: Store.mainLists
    };
    let mainLists = Store.mainLists;
    let newMainListName;
  }

  addNewMainList() {
    if (this.newMainListName) {
      let mainList = {
        name: this.newMainListName,
        id: new Date().valueOf(),
        createdOn: new Date(),
        mainTasks: []
      }
      this
        .state
        .mainLists
        .push(mainList);
      ReactDOM
        .findDOMNode(this.refs.mainListInput)
        .value = '';
      this.persistMainlists(this.state.mainLists);
      this.setState({mainLists: this.state.mainLists})
    }
  }

  persistMainlists(mainLists) {
    Store.setMainLists(mainLists);
    console.log('after persisting to global variable', window.mainLists);
  }

  _onEnter(event) {
    return event.key == 'Enter';
  }

  _setMainListName(event) {
    this.newMainListName = event.target.value;
    console.log('mainListName', this.newMainListName);
    if (this._onEnter(event)) {
      this.addNewMainList();
    }
  }

  componentWillUpdate() {
    console.log('willMount Called');
  }

  componentWillMount() {
    console.log('willMount Called');
  }
  componentDidMount() {
    console.log('componentDidMount Called');
  }

  componentWillReceiveProps(props) {
    console.log('componentWillReceiveProps Called');
  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate Called');
    return true;
  }

  componentWillUnmount() {
    console.log('componentWillUnmount Called');
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const {collapsed} = this.state;
    const navClass = collapsed
      ? 'side-nav-ul nav navbar-nav'
      : 'side-nav-ul nav navbar-nav slide-in';
    const mainLists = Store.mainLists;
    // Map through mainLists and return linked subTask List
    const mainListNode = mainLists.map((mainList) => {
      return (
        <li key={mainList.id}>
          <a onClick={(event) => this.props.setMainList(event)} id={mainList.id}>
            <i className='fa fa-bars'></i>
            <span className='list-name'>
              {mainList.name}
            </span>
          </a>
        </li>
      )
    });
    return (
      <div className='col-sm-3 col-md-3 col-xs-3 col-lg-3 side-menu'>
        <nav className='navbar navbar-default' role='navigation'>
          <NavHeader toggle={() => this.toggleCollapse()}/>
          <div className='side-menu-container'>
            <ul className={navClass}>
              <li class='main-list-add-li'>
                <div id='search' className='add-list panel-collapse collapse in'>
                  <div className='panel-body'>
                    <div className='navbar-form'>
                      <input
                        type='text'
                        ref='mainListInput'
                        onKeyPress={(event) => this._setMainListName(event)}
                        className='form-control'
                        placeholder='Add List'/>
                      <button
                        onClick={() => this.addNewMainList()}
                        className='btn btn-default add-btn'>
                        <i className='fa fa-check'></i>
                      </button>
                    </div>
                  </div>
                </div>
              </li>
              {mainListNode}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}