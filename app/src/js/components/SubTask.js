import DatePicker from 'react-bootstrap-date-picker';
import TimePicker from 'react-bootstrap-time-picker';
import React from 'react';
import ReactConfirmAlert, {confirmAlert} from 'react-confirm-alert'; // Import
import ReactDOM from 'react-dom';
import SubTaskElement from '../components/SubTaskElement';
import Store from '../store/Store';
import {IndexLink, Link} from 'react-router';
import NavHeader from './NavHeader';
export default class SubTask extends React.Component {
  constructor(props) {
    let newSubTaskName;
    let mainTask;
    super(props);
    console.log('props in subtask', this.props);

    this.state = {
      mainList: this.props.mainList,
      mainTask: this.props.mainTask,
      hideSubTask: false
    };
    console.log('props in subtask', this.props);
  }

  addNewSubTask() {
    if (this.newSubTaskName) {
      let subTask = {
        name: this.newSubTaskName,
        id: new Date().valueOf(),
        createdOn: new Date(),
        mainTasks: []
      }
      this
        .state
        .mainTask
        .subTasks
        .push(subTask);
      ReactDOM
        .findDOMNode(this.refs.subTaskInput)
        .value = '';
      this.setState({mainTask: this.state.mainTask})
    }
  }

  setSubTask(event) {
    if (event.target.id) {
      Store.setSubTask({
        mainListId: this.state.mainList.id,
        mainTaskId: this.state.mainTask.id,
        subTaskId: event.target.id,
        isCompleted: event.target.checked,
        type: 'update',
        liftState: (updatedMainTask) => {
          this.setState({mainTask: updatedMainTask})
        }
      });
    }
  }

  deleteSubTask(event) {
    if (event.target.id) {
      Store.setSubTask({
        mainListId: this.state.mainList.id,
        mainTaskId: this.state.mainTask.id,
        subTaskId: event.target.id,
        isCompleted: event.target.checked,
        type: 'delete',
        liftState: (updatedMainTask) => {
          this.setState({mainTask: updatedMainTask})
        }
      });
    }
  }

  _setSubTaskName(event) {
    this.newSubTaskName = event.target.value;
    console.log('SubTaskName', this.newSubTaskName);
  }
  componentWillUpdate() {
    console.log('willMount Called');
  }

  componentWillMount() {
    console.log('willMount Called in subTaskComponent');

  }
  componentDidMount() {
    this.state.mainTask = null;
    console.log('componentDidMount Called in subTaskComponent');

  }

  componentWillReceiveProps(props) {
    this.state = {
      mainList: props.mainList,
      mainTask: props.mainTask
    };
    console.log('props in subtasksssssssssss', this.props);
    console.log('componentWillReceiveProps Called');

  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate Called');

    return true;
  }

  componentWillUnmount() {
    console.log('componentWillUnmount Called');

  }
  confirmDelete() {
    confirmAlert({
      title: 'Confirm Delete Task ' + this.props.mainTask.name, // Title dialog
      message: 'Are you sure to do Delete task with ' + this.props.mainTask.subTasks.length + ' subTasks?', // Message dialog
      childrenElement: () => <div></div>, // Custom UI or Component
      confirmLabel: 'Delete', // Text button confirm
      cancelLabel: 'Cancel', // Text button cancel
      onConfirm: () => this
        .props
        .actions
        .deleteTask(this.props.mainTask.id), // Action after Confirm
      onCancel: () => console.log('canceled') // Action after Cancel
    })
  }
  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  hideSubTask() {
    this.setState({hideSubTask: true})
  }

  getFormattedDate(date) {
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    const days = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ];
    const formattedDate = new Date(date);
    console.log('formatted Date', formattedDate, formattedDate.getMonth(), formattedDate.getDay());
    return monthNames[formattedDate.getMonth()] + '-' + formattedDate.getDate() + '-' + days[formattedDate.getDay()];
  }
  render() {
    let subTaskNode;
    let formattedDate;
    const mainTask = this.props.mainTask;
    console.log('mainTask in subTask component', mainTask);
    if (mainTask && !this.state.hideSubTask) {
      if (mainTask.createdOn) {
        formattedDate = this.getFormattedDate(mainTask.createdOn);
      }
      const starClass = mainTask.isImportant
        ? 'fa fa-star big-star bg-red'
        : 'fa fa-star big-star';
      const strikeClass = mainTask.completed
        ? 'big-strike'
        : 'big-strike hidden';

      if (mainTask && mainTask.subTasks) {
        subTaskNode = mainTask
          .subTasks
          .map((subTask) => {
            return (
              <li key={subTask.id}>
                <span class='subtask-options'>
                  <input
                    className='box-15'
                    type='checkbox'
                    name='name'
                    value={subTask.completed}
                    defaultChecked={subTask.completed}
                    id={subTask.id}/>
                  <i id={subTask.id} class='fa fa-trash'></i>
                </span>
                <span className='list-name'>
                  {subTask.name}
                </span>
              </li>
            )
          });
      }
      return (
        <div class='col-sm-6 col-md-6 col-xs-6 col-lg-6 sub-task'>
          <div class='sub-task-add-content'>
            <div class='panel panel-default'>
              <div class='panel-heading'>
                <div ref='mainTaskStrike' class={strikeClass}></div>
                <span ref='mainTaskCheck' class='' id=''><input
                  class='chkbox-lg'
                  type='checkbox'
                  checked={mainTask.completed}
                  id={mainTask.id}
                  onClick={this.props.actions.setTaskStatus}/></span>
                <span class='sub-note-title'>
                  <span class='note-text' id='noteText'>{mainTask.name}</span>
                  <span ref='mainTaskStar' class='sub-task-head-star'>
                    <i
                      id={mainTask.id}
                      onClick={this.props.actions.setTaskImportant}
                      class={starClass}></i>
                  </span>
                  <span onClick={() => this.hideSubTask()} class='close-thick'></span>
                </span>
              </div>
              <div class='panel-body'>
                <div class=''>
                  <div class=''>
                    <div class='sub-task-li cl-blue'>
                      <i class='fa fa-calendar f-s-20'></i>
                      <DatePicker
                        className='date-picker'
                        id='example-datepicker'
                        value={mainTask.dueDate}
                        onChange={(event) => this.props.actions.setTaskDueDate(event)}/>
                    </div>
                    <div class='sub-task-li '>
                      <i class='fa fa-clock-o f-s-20'></i>
                      <TimePicker
                        value={mainTask.remindMe}
                        onChange={(event) => this.props.actions.setTaskReminder(event)}/>
                    </div>
                  </div>
                </div>
                <div class='add-sub-task'>
                  <div class='row add-sub-input'>
                    <div id='loginform' class='form-horizontal' role='form'>
                      <div className='m-b-25' class='input-group'>
                        <span class='input-group-addon'>
                          <i class='fa fa-plus'></i>
                        </span>
                        <input
                          id='login-username'
                          type='text'
                          class='form-control'
                          name='username'
                          placeholder='Add a subtask'/>
                      </div>
                    </div>
                  </div>
                </div>
                <textarea type="input" className="subtask-note"></textarea>
                <div class='paper-clip'>
                  <SubTaskElement
                    mainTask={mainTask}
                    deleteSubTask={(event) => this.deleteSubTask(event)}
                    setSubTask={(event) => this.setSubTask(event)}></SubTaskElement>
                </div>
              </div>
              <div class='panel-footer sub-task-footer'>
                <i
                  id={mainTask.id}
                  class='fa fa-trash f-s-20'
                  onClick={(mainTask) => this.confirmDelete(mainTask)}></i>
                <i class='fa fa-play f-s-20' onClick={() => this.hideSubTask()}></i>
              </div>
            </div>

          </div>
        </div>
      );
    } else {
      return (null);
    }
  }
}