import React from "react";
export default class SubTaskElement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainTask: this.props.mainTask
        }
    }

    componentWillReceiveProps(props) {
        if (props.mainTask) {
            this.state = {
                mainTask: props.mainTask
            }
        }

    }

    render() {
        let finished;
        let strikeClass;
        let subTaskNode;
        const mainTask = this.state.mainTask;
        if (mainTask && mainTask.subTasks) { // Map through mainTaks and seperate to finished and unfinished tasks
            const subTasks = mainTask.subTasks;
            subTaskNode = subTasks.map((subTask) => {
                strikeClass = subTask.completed
                    ? "strike"
                    : "strike hidden";
                return (
                    <div
                        key={subTask.id}
                        id={subTask.id}
                        className="note-item-btn sub-task-bg sub-item-btn">
                        <span className={strikeClass} id={subTask.id}></span>
                        <div className="note-checkbox" id={subTask.id}><input
                            className="mdm-chkbox box-15"
                            type="checkbox"
                            onClick={this.props.setSubTask}
                            name="name"
                            value={subTask.completed}
                            defaultChecked={subTask.completed}
                            id={subTask.id}/></div>
                        <div className="note-title" id={subTask.id}>
                            <span>{subTask.name}</span>
                        </div>
                        <div className="note-date" id={subTask.id}>
                            <span className="fl-right">
                                <i id={subTask.id} onClick={this.props.deleteSubTask} className="fa fa-trash"></i>
                            </span>
                        </div>
                    </div>
                )

            });
        }
        return (
            <div className="main-task-list sub-task-list">
                {subTaskNode}
            </div>
        );
    }
}