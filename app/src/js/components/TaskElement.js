import React from "react";
import {IndexLink, Link} from "react-router";

export default class TaskElement extends React.Component {
    getFormattedDate(date) {
        const monthNames = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];
        const days = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ];
        const formattedDate = new Date(date);
        console.log("formatted Date", formattedDate, formattedDate.getMonth(), formattedDate.getDay());
        return monthNames[formattedDate.getMonth()] + "-" + formattedDate.getDate();
    }
    render() {
        let finished;
        let starClass;
        let strikeClass;
        let tasksNode;
        const mainList = this.props.currentMainList;
        if (mainList && mainList.mainTasks) { // Map through mainTaks and seperate to finished and unfinished tasks
            const mainTasks = mainList.mainTasks;
            tasksNode = mainTasks.map((mainTask) => {
                finished = "false";
                if (mainTask.completed) {
                    finished = "true";
                }
                if (finished == this.props.finished) {
                    starClass = mainTask.isImportant
                        ? "fa fa-star p-l-20 bg-red"
                        : "fa fa-star p-l-20";
                    strikeClass = mainTask.completed
                        ? "strike"
                        : "strike hidden";
                    return (
                        <div
                            key={mainTask.id}
                            onClick={this.props.renderSubtask}
                            id={mainTask.id}
                            className="note-item-btn">
                            <span className={strikeClass} id={mainTask.id}></span>
                            <div className="note-checkbox" id={mainTask.id}><input
                                className="mdm-chkbox box-15"
                                type="checkbox"
                                onClick={this.props.setTaskStatus}
                                name="name"
                                value={mainTask.completed}
                                defaultChecked={mainTask.completed}
                                id={mainTask.id}/></div>
                            <div className="note-title" id={mainTask.id}>
                                <span>{mainTask.name}</span>
                            </div>
                            <div className="note-date" id={mainTask.id}>
                                <span className="fl-right">{this.getFormattedDate(mainTask.dueDate)}
                                    <i id={mainTask.id} onClick={this.props.setTaskImportant} className={starClass}></i>
                                </span>
                            </div>
                        </div>
                    )
                }
            });
        }
        return (
            <div className="main-task-list">
                {tasksNode}
            </div>
        );
    }
}