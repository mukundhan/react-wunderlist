import React from 'react';
import {Link} from 'react-router';
import SideNav from '../components/SideNav';
import MainContent from '../components/MainContent';
import Store from '../store/Store';
export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainLists: Store.mainLists,
      currentMainList: null
    }
    console.log('props', Store.mainLists);
  }
  setMainList(event) {
    let mainList;
    const mainListId = event.target.id;
    if (mainListId) {
      mainList = this
        .state
        .mainLists
        .filter(function (tempList) {
          return tempList.id == mainListId;
        })
      this.setState((prevState, props) => ({currentMainList: mainList[0]}));
    }
    console.log('founded mainList', mainList[0]);
  }

  render() {
    console.log('Index render');
    return (
      <div className='index'>
        <SideNav setMainList={(event) => this.setMainList(event)}/>
        <MainContent currentMainList={this.state.currentMainList}/>
      </div>
    );
  }
}