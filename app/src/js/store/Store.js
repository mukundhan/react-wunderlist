export default class Store {
  static mainLists = [];
  constructor(data) {
    Store.mainLists = data.mainLists;
  }
  static pushMainList(mainList) {
    Store
      .mainLists
      .push(mainList);
    console.log("after mainList Added", Store.mainLists);
  }
  static getMainLists() {
    return Store.lists;
  }
  static setMainLists(mainLists) {
    Store.mainLists = mainLists;
    console.log("after mainList updated ", Store.mainLists);
  }
  setMainList(mainList) {
    Store
      .mainLists
      .forEach((element, index, array) => {
        if (element.id == mainList.id) {
          element = mainList;
        }
      });
    Store.setMainLists(this.state.mainLists);
  }
  static setMainTask(mainListId, mainTask, type) {
    Store
      .mainLists
      .forEach((list) => {
        if (list.id == mainListId) {
          if (type == "update") {

            list
              .mainTasks
              .forEach((task) => {
                if (task.id == mainTask.id) {
                  task = mainTask;
                }
              });
          } else if (type == "add") {
            list
              .mainTasks
              .push(mainTask);
          }
        }
      });
    console.log("after maintask updated ", Store.mainList);
  }
  static setSubTask(object) {
    let tempSubTaskId = -2;
    Store
      .mainLists
      .forEach((list) => {
        if (list.id == object.mainListId) {
          list
            .mainTasks
            .forEach((task) => {
              if (task.id == object.mainTaskId) {
                task
                  .subTasks
                  .forEach((tempSubTask, index, array) => {
                    if (tempSubTask.id == object.subTaskId) {
                      if (object.type == "update") {
                        tempSubTask.completed = object.isCompleted;
                        object.liftState(task);
                      } else if (object.type == "delete") {
                        tempSubTaskId = index;
                      }
                    }
                  })
                if (tempSubTaskId > -1) {
                  task
                    .subTasks
                    .splice(tempSubTaskId, 1);
                  object.liftState(task);
                }

              }
            })
        }
      })
  }

}
